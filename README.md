## Assignment Blue Machine: Core Framework
based on Laravel 9, PHP 8.1

### Description
Backend - Laravel Framwork
Frontend - React.js

### Prerequisite
- Docker (https://www.docker.com/products/docker-desktop)
- PHP 8.1
- Composer
- Node

### Install project and run docker
```
1. git clone 
2. cd blue-machine
3. docker-compose up -d --build
```

### Install vendor and create database
Run this command inside docker container
- install vendor
- create database
- make data into database
### Blue_machine_app [container]
```
1. composer install
2. php artisan migrate
3. php artisan db:seed
```

### Assets build
```
# install packages
1. cd blue-machine
2. npm install

# development
npm run dev

# build
npm run build
```

