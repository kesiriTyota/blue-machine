/** @type {import('tailwindcss').Config} */
const colors = require('tailwindcss/colors');
module.exports = {
  content: [
    "./resources/**/*.blade.php",
    "./resources/**/*.js",
    "./resources/**/*.jsx",
  ],
  theme: {
    fontFamily: {
      'sofia': ['Sofia Sans', 'Roboto', 'serif'],
      'roboto': ['Roboto', 'serif'],
    },
    extend: {
      backgroundImage: {
        'screen-pattern': "linear-gradient(180deg, #d6d6d600 50%, rgb(212 212 212 / 30%) 51%)",
        'screen-pattern-blue': "linear-gradient(180deg, #84a8d6 50%, rgb(185 207 236) 51%)",
        'screen-pattern-black': "linear-gradient(180deg, #18a420 50%, rgb(9 90 17 / 43%) 51%)",
      },
      colors: {
        'primary': '#262f8d',
        'primary_2': '#f90083',
        'primary_3': '#ff341d',
        'primary_4': '#fe5f0f',
        'primary_5': '#184cb4',
      },
    },
  },
  plugins: [],
}
//Sofia Sans
