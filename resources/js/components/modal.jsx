import React from 'react';
import Loading from '../components/loading'

const Modal = (props) => {
    const {
        payment,
        pickProduct,
        msgCalculate,
        isPurchaseLoading,
        response,
        onClickCancel,
        onClickPayment
    } = props;
    return (<div className="mx-4">
            <div className="absolute z-[9] inset-0 bg-black bg-opacity-40 transition-opacity" />
            <div className="absolute flex flex-col justify-start min-h-[360px] z-50 mx-4 w-[360px] left-0 transform overflow-hidden rounded-lg bg-white px-4 pt-5 pb-4 text-left shadow-xl transition-all sm:p-6">
                {
                    !payment ?
                        <div>
                            <div className="mx-auto flex h-50 w-50 items-center justify-center rounded-full bg-blue-50">
                                <img src={pickProduct.picture} className="w-[180px] object-cover object-center"/>
                            </div>
                            <div className="mt-2 text-center">
                                <div className="text-lg font-medium leading-6 text-gray-900">
                                    {pickProduct.name}
                                </div>
                            </div>
                            {!payment ? <div
                                className="mt-1 text-center text-lg text-gray-500"> ฿{pickProduct.price} </div> : ""}
                        </div>
                        : <>
                            <div className="mx-auto flex h-50 w-50 items-center justify-center rounded-full bg-blue-50">
                                <img src={pickProduct.picture} className="w-[100px] object-cover object-center"/>
                            </div>
                            <div className="">
                                <div className="flex items-center justify-between border-b border-gray-200 pt-4">
                                    <dt className="text-base font-medium text-gray-900">Total</dt>
                                    <dd className="text-base font-medium text-gray-900">฿{pickProduct.price}</dd>
                                </div>
                                <div className="flex items-center justify-between border-b border-gray-200 pt-4">
                                    <dt className="text-base font-medium text-gray-900">
                                        <div>{msgCalculate.current}</div>
                                    </dt>
                                    <dd className="text-base font-medium text-gray-900">
                                        <img src="/svg/money-bracket.svg" className="w-[22px]"/>
                                    </dd>
                                </div>
                            </div>
                          </>

                }

                { (isPurchaseLoading) ? <Loading/> :
                    response &&
                    <div className="">
                        {
                            response.success ?
                                <>
                                    <div className="mt-5 mb-4 flex gap-3 items-center justify-center">
                                        <div
                                            className="flex h-8 w-8 items-center justify-center rounded-full bg-green-100">
                                            <img src="/svg/correct.svg" className="h-4 w-4 text-green-600"/>
                                        </div>
                                        <div className="text-lg font-medium text-gray-900"> {response.message} </div>
                                    </div>
                                    {
                                        response.data.length > 0 &&
                                        <div className="rounded-md bg-gray-100 p-4">
                                            <div className="mt-1 flex items-center justify-between font-semibold">
                                                <dt className="text-md text-gray-900">Change Money</dt>
                                                <dd className="text-md text-gray-900">Amount</dd>
                                            </div>
                                            {
                                                response.data.map((data, key) => (
                                                    <div key={key} className="flex py-2 items-center justify-between border-t border-gray-200">
                                                        <dt className="text-sm text-gray-600">{ data.value }</dt>
                                                        <dd className="text-sm font-medium text-gray-600">{ data.amount }</dd>
                                                    </div>
                                                ))
                                            }
                                        </div>
                                    }
                                </>
                                :
                                <>
                                    <div className="mt-5 mb-4 flex gap-3 items-center justify-center">
                                        <div className="flex h-8 w-8 items-center justify-center rounded-full bg-red-100">
                                            <img src="/svg/warning.svg" className="h-4 w-4 text-red-600"/>
                                        </div>
                                        <div className="text-lg font-medium text-gray-900"> {response.message} </div>
                                    </div>
                                </>
                        }

                        <div>
                            <button type="button"
                                    className="mt-2 inline-flex w-full justify-center rounded-md border border-transparent bg-gray-200 px-4 py-2 text-md font-medium text-gray-500 shadow-sm hover:bg-gray-300 focus:outline-none focus:ring-2 focus:ring-gray-200 focus:ring-offset-2 sm:text-sm"
                                    onClick={() => onClickCancel()}>
                                CLOSE
                            </button>
                        </div>
                    </div>
                }


                { !payment &&
                    <div className="mt-5 sm:mt-6 sm:grid sm:grid-flow-row-dense sm:grid-cols-2 sm:gap-3">
                        <button type="button"
                                className="inline-flex w-full justify-center rounded-md border border-transparent bg-gray-200 px-4 py-2 text-md font-medium text-gray-500 shadow-sm hover:bg-gray-300 focus:outline-none focus:ring-2 focus:ring-gray-200 focus:ring-offset-2 sm:text-sm"
                                onClick={() => onClickCancel()}>
                            CANCEL
                        </button>
                        <button type="button"
                                className="inline-flex w-full justify-center rounded-md border border-transparent bg-blue-500 px-4 py-2 text-md font-medium text-white shadow-sm hover:bg-blue-600 focus:outline-none focus:ring-2 focus:ring-blue-400 focus:ring-offset-2 sm:text-sm"
                                onClick={() => onClickPayment()}>
                            BUY
                        </button>
                    </div>
                }

            </div>
        </div>
    );
}

export default Modal;