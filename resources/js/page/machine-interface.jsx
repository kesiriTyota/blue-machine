import React from 'react';
import {createRoot} from 'react-dom/client'
import useFetchPurchase from "../hooks/useFetchPurchase";
import axios from 'axios';
import {useEffect, useState, useRef, Fragment} from 'react'
import Modal from '../components/modal'
import useFetchProducts from "../hooks/useFetchProducts";
import useFetchMoneyType from "../hooks/useFetchMoneyType";

export default function Machine() {
    const [pickProduct, setPickProduct] = useState(null);
    const [openInput, setOpenInput] = useState(false)
    const [money, setMoney] = useState(0);
    const [payment, setPayment] = useState(false)
    const [insertMoney, setInsertMoney] = useState([]);
    const msgCalculate = useRef('Please insert money');

    //custom hook
    const { data: response, loading: isPurchaseLoading, setData: setResponse, fetchData } = useFetchPurchase();
    const { data: products } = useFetchProducts();
    const { data: moneyType } = useFetchMoneyType();

    const onclickPickProduct = (product) => {
        setPickProduct(product);
    }

    const onClickPayment = () => {
        setPayment(true)
        setOpenInput(true)
    }

    const onClickCancel = () => { // Reset
        setPickProduct(null)
        setPayment(false)
        setOpenInput(false)
        setMoney(0)
        setInsertMoney([])
        setResponse(null)
        msgCalculate.current = "Please insert money";
    }

    const InsertMoney = (insert_money) => {
        /* collect insert money */
        const cloneInsertMoney = JSON.parse(JSON.stringify((insertMoney)));
        const isExist = insertMoney.find(el => el.id === insert_money.id);

        if (isExist) {
            cloneInsertMoney.forEach((el) => {
                if (el.id === insert_money.id) {
                    el.amount += 1;
                }
            })
        } else {
            cloneInsertMoney.push({...insert_money, amount: 1})
            // setInsertMoney([...cloneArray, {...insert_money, amountClick: 1}])
        }
        setInsertMoney(cloneInsertMoney)

        /* calculate money */
        setMoney(money + insert_money.value)
        let summary = pickProduct?.price - (money + insert_money.value);
        if (summary > 0) {
            msgCalculate.current = `Left ${summary} Bath`;
        } else {
            let money_change = Math.abs(summary)
            msgCalculate.current = `Change ${money_change} Bath`
            /* Call api purchase */
            setOpenInput(false)
            fetchData(cloneInsertMoney, money_change, pickProduct.id)
        }
    }

    return (<>
        {/*max-w-[1000px] mx-auto py-10 px-2*/}
        <div className="max-w-[800px] mx-auto py-10 px-2">
            <div className="bg-white">
                <div className="flex h-full flex-col border border-gray-200">
                    <header className="flex flex-none bg-gray-800 items-center justify-between py-4 px-6">
                        <div>
                            <div className="font-roboto text-lg font-semibold leading-6 text-white">
                                Blue Machine
                            </div>
                            {/*<p className="mt-1 text-sm text-gray-500">Saturday</p>*/}
                        </div>
                    </header>
                    <div className="isolate flex flex-auto overflow-hidden bg-white">
                        <div className="flex flex-auto flex-col overflow-auto">
                            <div className="relative px-8 py-6 bg-screen-pattern bg-[length:100%_2px]">
                                {pickProduct ? <Modal
                                    payment={payment}
                                    pickProduct={pickProduct}
                                    msgCalculate={msgCalculate}
                                    isPurchaseLoading={isPurchaseLoading}
                                    response={response}
                                    onClickCancel={onClickCancel}
                                    onClickPayment={onClickPayment} /> : ""}
                                <div
                                    className="grid grid-cols-1 gap-x-6 gap-y-4 sm:grid-cols-2 lg:grid-cols-3 lg:gap-x-8">
                                    {products ? products.map((product, key) => (
                                            <button onClick={() => onclickPickProduct(product)} key={key}
                                                    className="group relative">
                                                <div
                                                    className="aspect-w-1 aspect-h-1 overflow-hidden rounded-md bg-gray-200 bg-opacity-80 group-hover:bg-blue-100 shadow-sm">
                                                    <img src={product.picture}
                                                         className="w-[170px] object-cover object-center"/>
                                                </div>
                                                <div className="flex justify-between py-1 text-sm w-full text-blue-800">
                                                    <div className="text-gray-800">{product.name}</div>
                                                    <div>
                                                        <span className="text-sm">฿</span>{product.price}
                                                    </div>
                                                </div>
                                            </button>
                                        ))
                                        : <div className="col-span-3 text-lg text-center h-[420px]">
                                            Loading...
                                        </div>
                                    }
                                </div>
                            </div>
                        </div>
                        <div className="w-1/2 max-w-md flex-none border-l border-gray-100">
                            <div className="col-span-2">
                                <div className="py-4 px-8">
                                    <div className="my-3 text-md font-semibold">Digital Input Money</div>
                                    <div className="min-h-[100px] p-2 text-gray-800 rounded-md bg-gray-600 bg-[length:100%_2px] border border-gray-200">
                                        {
                                            insertMoney.length > 0 &&
                                            <>
                                                <div className="flex items-center text-md text-white justify-between font-semibold">
                                                    <dt>Money</dt>
                                                    <dd>Amount</dd>
                                                </div>
                                                {
                                                    insertMoney.map((data, key) => (
                                                        <div key={key} className="flex items-center justify-between font-semibold">
                                                            <dt className="flex gap-2 text-md text-white">
                                                                {
                                                                    data.type === "coin" ?
                                                                        <img src="/svg/coin_3.svg" className="w-[12px]"/> :
                                                                        <img src="/svg/banknote.svg" className="w-[15px]"/>
                                                                }
                                                                { data.value }
                                                            </dt>
                                                            <dd className="text-md text-white">{data.amount}</dd>
                                                        </div>
                                                    ))
                                                }
                                            </>
                                        }
                                    </div>
                                    <div className="mt-3 text-md font-semibold">Input Money</div>
                                    <div className="mt-2 grid grid-cols-3 gap-2">
                                        {
                                            moneyType && moneyType.map((money, key) => (
                                                    <div key={key} className="col-span-3 sm:col-span-1 text-center">
                                                        <button onClick={() => {
                                                            InsertMoney(money)
                                                        }} disabled={!openInput}
                                                                className="w-full flex gap-2 items-center text-white px-6 py-2 bg-primary rounded-lg hover:opacity-80 cursor-pointer shadow-sm disabled:bg-gray-400 disabled:cursor-not-allowed">
                                                            {
                                                                money.type === "coin" ?
                                                                    <img src="/svg/coin_3.svg" className="w-[15px] text-white"/> :
                                                                    <img src="/svg/banknote.svg" className="w-[20px] text-white"/>
                                                            }
                                                            {money.value}
                                                        </button>
                                                    </div>
                                                ))
                                        }
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </>);
}

if (document.getElementById('root')) {
    createRoot(document.getElementById('root')).render(<Machine/>)
}