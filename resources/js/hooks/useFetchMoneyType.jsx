import axios from "axios";
import {useEffect, useState} from "react";

const useFetchMoneyType = () => {
    const [data, setData] = useState();
    const [loading, setLoading ]= useState(false);
    const [error, setError] = useState('');

    const fetchData = async () => {
        try {
            const {data: {data}} = await axios.get('/api/money-types')
            setData(data);
        } catch (error) {
            console.log("Fetch Money Type error", error)
        }
    }

    useEffect(()=>{
        fetchData();
    },[])

    return {
        data, loading, error
    }
}

export default useFetchMoneyType;