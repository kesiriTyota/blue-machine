import { useState, useEffect}  from 'react';
import axios from "axios";

const useFetchPurchase = () => {
    const [data, setData] = useState();
    const [loading, setLoading ]= useState(false);
    const [error, setError] = useState('');

    const fetchData = async (insertMoney, moneyChange, id) => {
        try {
            console.log("insertMoney", insertMoney)
            setLoading(true);
          const { data } =  await axios.post(`/api/purchase/${id}`,
                {
                    insert_money: insertMoney,
                    money_change: moneyChange
                });
            setData(data);
        } catch (error) {
            setData(null);
            setError(error.message);
            console.log("Fetch Purchase error", error)
        } finally {
           setTimeout(() => {
               setLoading(false);
           },1000)
        }
    }

    return {
        data, loading, error, setData, fetchData
    }
}

export default useFetchPurchase;