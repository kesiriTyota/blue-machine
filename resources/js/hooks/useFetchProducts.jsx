import React from 'react';
import axios from "axios";
import {useEffect, useState} from "react";

const useFetchProducts = () => {
    const [data, setData] = useState();
    const [loading, setLoading ]= useState(false);
    const [error, setError] = useState('');

    const fetchData = async () => {
        try {
            const {data: {data}} = await axios.get('/api/products')
            setData(data);
        } catch (error) {
            console.log("Fetch Product error", error)
        }
    }

    useEffect(()=>{
        fetchData();
    },[])

    return {
        data, loading, error
    }
}

export default useFetchProducts;