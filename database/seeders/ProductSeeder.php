<?php

namespace Database\Seeders;

use App\Models\Product;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class ProductSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Coke',
                'price' => 20,
                'quantity' => 3,
                'picture' => "/images/01.png",
            ],
            [
                'id' => 2,
                'name' => 'Fanta',
                'price' => 15,
                'quantity' => 3,
                'picture' => "/images/02.png",
            ],
            [
                'id' => 3,
                'name' => 'Coke zero',
                'price' => 25,
                'quantity' => 3,
                'picture' => "/images/03.png",
            ],
            [
                'id' => 4,
                'name' => 'Christal',
                'price' => 15,
                'quantity' => 3,
                'picture' => "/images/04.png",
            ],
            [
                'id' => 5,
                'name' => 'Jele',
                'price' => 25,
                'quantity' => 3,
                'picture' => "/images/05.png",
            ],
            [
                'id' => 6,
                'name' => 'Nori',
                'price' => 25,
                'quantity' => 3,
                'picture' => "/images/06.png",
            ],
            [
                'id' => 7,
                'name' => 'Papica',
                'price' => 42,
                'quantity' => 3,
                'picture' => "/images/07.png",
            ],
            [
                'id' => 8,
                'name' => 'Lays',
                'price' => 59,
                'quantity' => 3,
                'picture' => "/images/08.png",
            ],
            [
                'id' => 9,
                'name' => 'Pocky',
                'price' => 26,
                'quantity' => 3,
                'picture' => "/images/09.png",
            ],
            [
                'id' => 10,
                'name' => 'Kitkat',
                'price' => 36,
                'quantity' => 3,
                'picture' => "/images/10.png",
            ]
        ];

        /* Create products */
        Product::insert($data);
    }
}
