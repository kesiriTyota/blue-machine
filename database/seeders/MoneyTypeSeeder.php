<?php

namespace Database\Seeders;

use App\Models\MoneyType;
use Illuminate\Database\Seeder;

class MoneyTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'value' => 1,
                'type' => 'coin'
            ],[
                'id' => 2,
                'value' => 5,
                'type' => 'coin'
            ],[
                'id' => 3,
                'value' => 10,
                'type' => 'coin'
            ],[
                'id' => 4,
                'value' => 20,
                'type' => 'banknote'
            ],[
                'id' => 5,
                'value' => 50,
                'type' => 'banknote'
            ],[
                'id' => 6,
                'value' => 100,
                'type' => 'banknote'
            ],[
                'id' => 7,
                'value' => 500,
                'type' => 'banknote'
            ],[
                'id' => 8,
                'value' => 1000,
                'type' => 'banknote'
            ],
        ];

        /* Create money type  */
        MoneyType::insert($data);
    }
}
