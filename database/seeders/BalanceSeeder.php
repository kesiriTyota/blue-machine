<?php

namespace Database\Seeders;

use App\Models\Balance;
use Illuminate\Database\Seeder;

class BalanceSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'money_type_id' => 1,
                'amount' => 10
            ],
            [
                'money_type_id' => 2,
                'amount' => 10
            ],
            [
                'money_type_id' => 3,
                'amount' => 10
            ],
            [
                'money_type_id' => 4,
                'amount' => 10
            ],
            [
                'money_type_id' => 5,
                'amount' => 10
            ],
            [
                'money_type_id' => 6,
                'amount' => 10
            ],
            [
                'money_type_id' => 7,
                'amount' => 10
            ],
            [
                'money_type_id' => 8,
                'amount' => 10
            ],
        ];

        /* Create money type  */
        Balance::insert($data);
    }
}
