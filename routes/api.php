<?php

use App\Http\Controllers\Api\MoneyTypeController;
use App\Http\Controllers\Api\ProductController;
use App\Http\Controllers\Api\PurchaseController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::get('products',  [ProductController::class, 'getProducts']);
Route::get('money-types',  [MoneyTypeController::class, 'getMoneyTypes']);
Route::post('purchase/{id}',  [PurchaseController::class, 'purchase']);

