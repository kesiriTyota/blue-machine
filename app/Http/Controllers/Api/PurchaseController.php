<?php


namespace App\Http\Controllers\Api;


use App\Models\Balance;
use App\Models\MoneyType;
use App\Models\Product;
use App\Models\Purchase;
use App\Models\PurchaseHasMoneyType;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class PurchaseController
{
    /**
     * @param  Request  $request
     * @param $id  //product ID
     * @return JsonResponse
     * TODO check stock available and calculate change money
     */
    public function purchase(Request $request, $id): JsonResponse
    {
        $insertMoney = $request->post('insert_money'); //user insert money
        $moneyChange = $request->post('money_change'); //amount money for change (calculate form frontend)
        try {
            $product = Product::where('id', $id)->where('quantity', '>', 0)->first();

            /* Check product available */
            if ($product) {

                if ($moneyChange == 0) {
                    /* The amount to be paid is just right, no change */
                    $this->updateHistory($id, $product, $insertMoney);
                    $data = null;
                } else {
                    /* calculate for change money */
                    $calculate = $this->calculate($moneyChange);
                    $data = $calculate["change"];

                    /* if money enough for change will update history */
                    if ($calculate['money'] == 0) {
                        $this->updateHistory($id, $product, $insertMoney, $data);
                    } else {
                        return response()->json([
                            'success' => false,
                            'data' => null,
                            'message' => "Sorry, Money not enough for change."
                        ], 200);
                    }
                }

                return response()->json([
                    'success' => true,
                    'data' => $data,
                    'message' => "Purchase Success"
                ], 200);

            } else {
                return response()->json([
                    'success' => false,
                    'data' => null,
                    'message' => 'product not available.'
                ], 200);
            }
        } catch (\Exception $e) {
            return response()->json([
                'success' => false,
                'data' => null,
                'message' => $e->getMessage()
            ], 200);
        }
    }

    /**
     * @param $id
     * @param $product
     * @param $insertMoney
     * @param $changeToUser
     * TODO Insert History Purchase in Database and Update Coin/Banknote in Balance Machine
     */
    private function updateHistory($id, $product, $insertMoney, $changeToUser = null)
    {
        /* Save purchase product  */
        $purchase = new Purchase();
        $purchase->product_id = $id;
        $purchase->price = $product->price;
        $purchase->amount = 1;
        $purchase->save();
        /* Add purchase has money type */
        foreach ($insertMoney as $item) {
            $purchaseHasMoney = new PurchaseHasMoneyType();
            $purchaseHasMoney->purchase_id = $purchase->id;
            $purchaseHasMoney->money_type_id = $item['id'];
            $purchaseHasMoney->amount = $item['amount'];
            $purchaseHasMoney->save();
            /* Update insert amount Coin/Banknote in machine */
            Balance::where('money_type_id', $item['id'])
                ->update([
                    'amount' => DB::raw("amount+".$item['amount']),
                ]);
        }

        /* Update Coin/Banknote in machine (change to user) */
        foreach ($changeToUser as $item) {
            Balance::where('money_type_id', $item['id'])
                ->update([
                    'amount' => DB::raw("amount-".$item['amount']),
                ]);
        }

    }

    /**
     * @param $money
     * TODO calculate to change money
     * Description: this function will change the money that is very valuable first.
     * If the money is not enough for change will send a message.
     * @return array
     */
    private function calculate($money): array
    {
        /* Query balance in machine */
        $balance = Balance::where('amount', '>', 0)->get()->sortByDesc('moneyType.value')->toArray();
        $numMoney = $money;
        $change = []; // collect money will change (value, amount)
        $isNotEnough = false;

        /* Loop money until charge = 0 or not enough for change */
        while ($numMoney > 0 && !$isNotEnough) {
            /* Loop money balance */
            foreach ($balance as $key => $item) {
                $value = $item['money_type']['value'];
                $amount = $item['amount'];
                /* Change money that is less than or equal */
                if ($numMoney >= $value) {
                    $changeAmount = !empty($change[$value]['amount']) ? $change[$value]['amount'] + 1 : 1;

                    /* Subtract the amount to be change from the amount already changed,
                       will get the amount to change in the next loop */
                    $numMoney = $numMoney - $value;
                    /* Stack value money of change */
                    $change[$value]['id'] = $item['money_type_id'];
                    $change[$value]['value'] = $value;
                    $change[$value]['amount'] = $changeAmount;

                    /* check balance machine if amount reaching the limited will remove from array */
                    if ($change[$value]['amount'] == $amount) {
                        /* check if reaching the lowest currency value and reaching the limit then stop loop */
                        if (end($balance)['money_type']['value'] == $value) {
                            $isNotEnough = true;
                            break;
                        }
                        unset($balance[$key]);
                    }
                    break;
                }
            }
        }

        /* Reformat Change Money */
        $reFormatChange = [];
        $count = 0;
        foreach ($change as $item){
            $reFormatChange[$count]['id'] = $item['id'];
            $reFormatChange[$count]['value'] = $item['value'];
            $reFormatChange[$count]['amount'] = $item['amount'];
            $count++;
        }

        return ['money' => $numMoney, 'change' => $reFormatChange];
    }


}