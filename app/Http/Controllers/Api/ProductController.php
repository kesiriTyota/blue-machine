<?php


namespace App\Http\Controllers\Api;


use App\Http\Resources\ProductCollection;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ProductController
{

    /**
     * @return ProductCollection
     */
    public function getProducts(): ProductCollection
    {
        $products = Product::where('status', 'active')->get();
        return new ProductCollection($products);
    }

}