<?php


namespace App\Http\Controllers\Api;


use App\Http\Resources\ProductCollection;
use App\Models\MoneyType;
use App\Models\Product;
use Illuminate\Http\JsonResponse;

class MoneyTypeController
{

    /**
     * @return JsonResponse
     */
    public function getMoneyTypes(): JsonResponse
    {
        $moneyType = MoneyType::all();
        return response()->json(['data' => $moneyType]);
    }
}