<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;

class Balance extends Model
{
    use HasFactory;

    /**
     * @return HasOne
     */
    public function moneyType(): HasOne
    {
        return $this->hasOne(MoneyType::class, 'id','money_type_id');
    }
}
